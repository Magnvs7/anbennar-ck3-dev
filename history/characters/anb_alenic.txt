﻿62 = { # Rylen of Adshaw
	name = "Rylen"
	dna = 62_rylen_adshaw
	dynasty = 29 #Adshaw
	religion = "castanorian_pantheon"
	culture = "old_alenic"
	
	trait = race_human
	trait = vengeful
	trait = impatient
	trait = arbitrary
	trait = education_martial_3
	
	980.4.6 = {
		birth = yes
		effect = {
			add_character_flag = has_scripted_appearance
		}
	}
	
	998.8.2 = {
		add_spouse = 62
	}
}

30000 = { # Peter Goldeneyes
	dna = 30000_peter_goldeneyes
	name = "Peter"
	dynasty = dynasty_goldeneyes
	religion = castanorian_pantheon
	culture = old_alenic

	trait = calm
	trait = brave
	trait = stubborn
	trait = education_martial_4
	trait = berserker
	trait = race_human
	trait = unyielding_defender
	
	998.4.4 = {
		birth = yes
	}
	
	1021.06.27 = {
		effect = {
			set_relation_rival = character:30000 # Peter Goldeneyes
		}
	}
}

30001 = { # Betram Frostwall
	dna = 30001_betram_frostwall
	name = "Betram"
	dynasty = dynasty_frostwall
	religion = castanorian_pantheon
	culture = old_alenic

	trait = content
	trait = callous
	trait = arrogant
	trait = education_martial_4
	trait = race_human
	
	986.10.6 = {
		birth = yes
	}
	
	1018.6.2 = {
		add_spouse = 30002
	}
	
	1021.06.27 = {
		add_pressed_claim = title:c_frostwall
		effect = {
			set_relation_rival = character:30000 # Peter Goldeneyes
		}
	}
	
	1021.09.12 = {
		employer = 62
		give_council_position = councillor_marshal
	}
}

30002 = { # Bertam wife Mechthild
	name = "Annabel"
	religion = castanorian_pantheon
	dynasty = 29 #Adshaw
	culture = old_alenic
	female = yes

	trait = impatient
	trait = ambitious
	trait = arrogant
	trait = education_diplomacy_3
	trait = race_human
	
	father = 62
	
	1000.10.6 = {
		birth = yes
	}
	
	1018.6.2 = {
		add_spouse = 30001
	}
}

30003 = { # Betram child 1
	name = "Adrian"
	dynasty = dynasty_frostwall
	religion = castanorian_pantheon
	culture = old_alenic
	father = 30001
	mother = 30002
	
	1019.12.4 = {
		birth = yes
	}
}

30004 = { # Toste of Everwharf
	dna = 30004_toste_everwharf
	name = "Toste"
	religion = castanorian_pantheon
	culture = blue_reachman

	trait = greedy
	trait = deceitful
	trait = lustful
	trait = education_intrigue_2
	trait = race_human
	
	978.8.18 = {
		birth = yes
	}
	
	1006.12.1 = {
		add_spouse = 30005
	}
}

30005 = { # Toste wife Hrothwina
	name = "Hrothwina"
	religion = castanorian_pantheon
	culture = old_alenic
	female = yes
	
	986.9.14 = {
		birth = yes
	}
	
	1006.12.1 = {
		add_spouse = 30004
	}
}

30006 = { # Toste child 1
	name = "Eardwulf"
	religion = castanorian_pantheon
	culture = blue_reachman
	female = yes
	father = 30004
	mother = 30005
	
	1009.4.9 = {
		birth = yes
	}
}

30007 = { # Toste child 2
	name = "Camir"
	religion = castanorian_pantheon
	culture = blue_reachman
	father = 30004
	mother = 30005
	
	1012.6.22 = {
		birth = yes
	}
}

30008 = { # Toste child 3
	name = "Lan"
	religion = castanorian_pantheon
	culture = blue_reachman
	father = 30004
	mother = 30005
	
	1019.7.14 = {
		birth = yes
	}
}

30009 = { # Niklas of Reachspier
	dna = 30009_nikla_reachspier
	name = "Niklas"
	religion = castanorian_pantheon
	culture = blue_reachman
	
	trait = impatient
	trait = compassionate
	trait = vengeful
	trait = education_diplomacy_1
	trait = race_human

	1000.3.4 = {
		birth = yes
	}
}

30010 = { # Valdemar of Coldmarket
	dna = 300010_valdemar_coldmarket
	name = "Valdemar"
	religion = castanorian_pantheon
	culture = blue_reachman
	
	trait = craven
	trait = patient
	trait = ambitious
	trait = education_intrigue_3
	trait = race_human

	974.4.5 = {
		birth = yes
	}
	
	1004.10.9 = {
		add_spouse = 30011
	}
}

30011 = { # Valdemar Wife
	name = "Coraline"
	religion = castanorian_pantheon
	culture = blue_reachman
	female = yes

	974.7.15 = {
		birth = yes
	}
	
	1004.10.9 = {
		add_spouse = 30010
	}
	
	1020.6.5 = {
		death = {
			death_reason = death_childbirth
		}
	}
}

30012 = { # Valdemar Child 1
	name = "Adrian"
	religion = castanorian_pantheon
	culture = blue_reachman
	father = 30010
	mother = 30011
	
	1006.7.14 = {
		birth = yes
	}
}

30013 = { # Valdemar Child 2
	name = "Irminburg"
	female = yes
	religion = castanorian_pantheon
	culture = blue_reachman
	father = 30010
	mother = 30011
	
	1010.7.14 = {
		birth = yes
	}
}

30014 = { # Valdemar Child 3
	name = "Bertha"
	female = yes
	religion = castanorian_pantheon
	culture = blue_reachman
	father = 30010
	mother = 30011
	
	1016.7.14 = {
		birth = yes
	}
}

30015 = { # Valdemar Child 4
	name = "Clothilde"
	female = yes
	religion = castanorian_pantheon
	culture = blue_reachman
	father = 30010
	mother = 30011
	
	1020.6.5 = {
		birth = yes
	}
}

30016 = { # Tomar Adshaw (deceased)
	name = "Tomar"
	religion = castanorian_pantheon
	culture = old_alenic
	dynasty = 29 #Adshaw
	
	trait = race_human
	trait = patient
	trait = just
	trait = stubborn
	
	972.4.6 = {
		birth = yes
		effect = {
			add_character_flag = has_scripted_appearance
		}
	}
	1013.3.4 = {
		death = {
			death_reason = death_execution
			killer = 521 # Venac the Arrogant
		}
	}
}

30018 = { # Helena of Evnermark
	name = "Helena"
	religion = castanorian_pantheon
	culture = blue_reachman
	female = yes
	
	trait = race_human
	trait = arrogant
	trait = lazy
	trait = deceitful
	trait = education_intrigue_1
	trait = magical_affinity_1
	
	984.3.11 = {
		birth = yes
		set_sexuality = homosexual
	}
}

30019 = { # Mathias of Dinesk
	name = "Mathias"
	religion = castanorian_pantheon
	culture = blue_reachman # Not Old Alenic
	
	trait = race_human
	trait = patient
	trait = wrathful
	trait = arbitrary
	trait = education_diplomacy_1
	
	987.4.16 = {
		birth = yes
	}
	
	1006.2.22 = {
		add_spouse = 30020
	}
}

30020 = { # Geilana
	name = "Geilana"
	religion = castanorian_pantheon
	culture = blue_reachman
	female = yes
	
	trait = race_human
	
	988.1.20 = {
		birth = yes
	}
	
	1006.2.22 = {
		add_spouse = 30019
	}
}

30021 = { # Mathias Child 1
	name = "Asig"
	religion = castanorian_pantheon
	culture = blue_reachman
	father = 30019
	mother = 30020
	
	trait = race_human
	
	1006.12.29 = {
		birth = yes
	}
}

30022 = { # Mathias Child 2
	name = "Liudger"
	religion = castanorian_pantheon
	culture = blue_reachman
	father = 30019
	mother = 30020
	
	trait = race_human
	
	1012.6.1 = {
		birth = yes
	}
}

30023 = { # Mathias Child 3
	name = "Alain"
	religion = castanorian_pantheon
	culture = blue_reachman
	father = 30019
	mother = 30020
	
	trait = race_human
	
	1013.10.4 = {
		birth = yes
	}
}

30024 = { # Rylen Wife
	name = "Athana"
	religion = castanorian_pantheon
	culture = old_alenic
	female = yes
	
	trait = race_human
	
	980.4.6 = {
		birth = yes
	}
	
	998.8.2 = {
		add_spouse = 62
	}
}

30025 = { # Rylen Child 2 (First is married to Bertram)
	name = "Andrew"
	religion = castanorian_pantheon
	culture = old_alenic
	
	trait = race_human
	
	father = 62
	mother = 30024
	
	1004.1.16 = {
		birth = yes
	}
}

30026 = { # Rylen Child 3 (First is married to Bertram)
	name = "Branden"
	religion = castanorian_pantheon
	culture = old_alenic
	
	trait = race_human
	
	father = 62
	mother = 30024
	
	1012.2.12 = {
		birth = yes
	}
}