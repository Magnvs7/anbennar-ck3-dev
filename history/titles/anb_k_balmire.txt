k_balmire = {
	1000.1.1 = { change_development_level = 6 }
	1018.7.18 = {
		holder = 93 #Albert Balmire
	}
}

c_bal_mire = {
	1000.1.1 = { change_development_level = 7 }
}

d_falsemire = {
	1000.1.1 = { change_development_level = 8 }
}

c_falseharbour = {
	1000.1.1 = { change_development_level = 10 }
}

c_badeben = {
	1000.1.1 = { change_development_level = 9 }
}