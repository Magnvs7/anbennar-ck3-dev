k_deranne = {
	1000.1.1 = { change_development_level = 8 }
	980.1.1 = {
		holder = 7	#Kalbard sil Deranne
	}
}

c_westport = {
	1000.1.1 = { change_development_level = 10 }
}

c_deranne = {
	1000.1.1 = { change_development_level = 10 }
}

c_clearshore = {
	1000.1.1 = { change_development_level = 7 }
}

c_hollowview = {
	1000.1.1 = { change_development_level = 7 }
}

c_lencesk = {
	1000.1.1 = { change_development_level = 11 }
}

c_darom = {
	1000.1.1 = { change_development_level = 10 }
}

c_aelvar = {
	800.1.1 = {
		liege = "k_deranne"
	}
	990.07.02 = {
		holder = 156
	}
	1018.07.15 = {
		holder = 154
	}
}