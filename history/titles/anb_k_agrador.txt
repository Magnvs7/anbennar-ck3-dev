k_agrador = {
	1000.1.1 = { change_development_level = 8 }
}

c_agradord = {
	1000.1.1 = { change_development_level = 9 }
}

c_uelced = {
	1000.1.1 = { change_development_level = 9 }
}

c_athfork = {
	1000.1.1 = { change_development_level = 10 }
}

c_cannwood = {
	1000.1.1 = { change_development_level = 7 }
}

c_forksgrove = {
	1000.1.1 = { change_development_level = 7 }
}

c_westwatch = {
	1000.1.1 = { change_development_level = 9 }
}