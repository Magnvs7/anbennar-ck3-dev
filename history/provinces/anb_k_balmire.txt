#k_balmire
##d_bal_mire
###c_bal_mire
229 = {		#Bal Mire

	# Misc
	culture = adeanic
	religion = castanorian_pantheon
	holding = castle_holding

	# History
	1000.1.1 = {
		special_building_slot = castanorian_citadel_bal_mire_01
		special_building = castanorian_citadel_bal_mire_01
	}
}
2454 = {

    # Misc
    holding = church_holding

    # History

}
2455 = {

    # Misc
    holding = city_holding

    # History

}
2456 = {

    # Misc
    holding = none

    # History

}
2457 = {

    # Misc
    holding = none

    # History

}

###c_cravens_walk
231 = {		#Craven's Walk

	# Misc
	culture = adeanic
	religion = castanorian_pantheon
	holding = castle_holding

	# History
}
2447 = {

    # Misc
    holding = church_holding

    # History

}
2448 = {

    # Misc
    holding = none

    # History

}
2449 = {

    # Misc
    holding = none

    # History

}

###c_rottenstep
230 = {		#Rottenstep

	# Misc
	culture = adeanic
	religion = castanorian_pantheon
	holding = castle_holding

	# History
}
2450 = {

    # Misc
    holding = city_holding

    # History

}
2451 = {

    # Misc
    holding = none

    # History

}
2452 = {

    # Misc
    holding = none

    # History

}
2453 = {

    # Misc
    holding = none

    # History

}

###c_venomfen
232 = {		#Venomfen

	# Misc
	culture = adeanic
	religion = castanorian_pantheon
	holding = castle_holding

	# History
}
2458 = {

    # Misc
    holding = none

    # History

}
2459 = {

    # Misc
    holding = church_holding

    # History

}
2460 = {

    # Misc
    holding = none

    # History

}

##d_falsemire
###c_mireleigh
762 = {		#Mireleigh

	# Misc
	culture = adeanic
	religion = castanorian_pantheon
	holding = castle_holding

	# History
}
2461 = {

    # Misc
    holding = city_holding

    # History

}
2462 = {

    # Misc
    holding = none

    # History

}

###c_falseharbour
761 = {		#Falseharbour

	# Misc
	culture = adeanic
	religion = castanorian_pantheon
	holding = castle_holding

	# History
}
2463 = {

    # Misc
    holding = church_holding

    # History

}
2464 = {

    # Misc
    holding = city_holding

    # History

}

###c_badeben
759 = {		#Badeben

	# Misc
	culture = adeanic
	religion = castanorian_pantheon
	holding = castle_holding

	# History
}
2465 = {

    # Misc
    holding = none

    # History

}
2466 = {

    # Misc
    holding = city_holding

    # History

}
2467 = {

    # Misc
    holding = none

    # History

}

##d_middle_alen
###c_entalenham
2490 = {	#Entalenham

	# Misc
	culture = white_reachman
	religion = castanorian_pantheon
	holding = castle_holding

	# History
}
725 = {

    # Misc
    holding = church_holding

    # History

}
2489 = {

    # Misc
    holding = none

    # History

}

###c_mirelook
710 = {		#Mirelook

	# Misc
	culture = white_reachman
	religion = castanorian_pantheon
	holding = castle_holding

	# History
}
2487 = {

    # Misc
    holding = none

    # History

}

###c_headmans_wood
726 = { #Headman's Wood

	# Misc
	culture = white_reachman
	religion = castanorian_pantheon
	holding = castle_holding

	# History
}
2488 = {

    # Misc
    holding = none

    # History

}
