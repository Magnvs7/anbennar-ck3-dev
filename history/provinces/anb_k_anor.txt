#k_anor
##d_ardent_glade
###c_ardent_keep
865 = {		#Ardent Keep

	# Misc
	culture = castanorian
	religion = castanorian_pantheon
	holding = castle_holding

	# History
	1000.1.1 = {
		special_building_slot = white_walls_of_castanor_01
		special_building = white_walls_of_castanor_01
	}
}
2548 = {

    # Misc
    holding = none

    # History

}
2549 = {

    # Misc
    holding = city_holding

    # History

}
2550 = {

    # Misc
    holding = castle_holding

    # History

}

###c_eswall
869 = {		#Eswall

	# Misc
	culture = castanorian
	religion = castanorian_pantheon
	holding = castle_holding

	# History
	1000.1.1 = {
		special_building_slot = white_walls_of_castanor_01
		special_building = white_walls_of_castanor_01
	}
}
2545 = {

    # Misc
    holding = church_holding

    # History

}
2546 = {

    # Misc
    holding = none

    # History

}
2547 = {

    # Misc
    holding = city_holding

    # History

}

###c_cadells_rest
864 = {		#Cadell's Rest

	# Misc
	culture = castanorian
	religion = castanorian_pantheon
	holding = castle_holding

	# History
}
2541 = {

    # Misc
    holding = none

    # History

}
2542 = {

    # Misc
    holding = none

    # History

}

###c_grenmore
2543 = {

    # Misc
    culture = castanorian
    religion = castanorian_pantheon
	holding = castle_holding

    # History

}
2544 = {

    # Misc
    holding = church_holding

    # History

}

##d_oldhaven
###c_oldhaven
874 = {		#Oldhaven

	# Misc
	culture = castanorian
	religion = castanorian_pantheon
	holding = castle_holding

	# History
}
2539 = {

    # Misc
    holding = church_holding

    # History

}
2540 = {

    # Misc
    holding = none

    # History

}

###c_hagstow
873 = {		#Hagstow

	# Misc
	culture = castanorian
	religion = castanorian_pantheon
	holding = castle_holding

	# History
}
2537 = {

    # Misc
    holding = none

    # History

}
2538 = {

    # Misc
    holding = none

    # History

}

###c_annistoft
866 = {		#Annistoft

	# Misc
	culture = castanorian
	religion = castanorian_pantheon
	holding = castle_holding

	# History
}
2535 = {

    # Misc
    holding = city_holding

    # History

}
2536 = {

    # Misc
    holding = none

    # History

}

##d_bradsecker
###c_bradnath
854 = {		#Bradnath

	# Misc
	culture = black_castanorian
	religion = castanorian_pantheon
	holding = castle_holding

	# History
}
2531 = {

    # Misc
    holding = city_holding

    # History

}
2532 = {

    # Misc
    holding = none

    # History

}
2533 = {

    # Misc
    holding = city_holding

    # History

}

###c_mintirley
844 = {		#Mintirley

	# Misc
	culture = black_castanorian
	religion = castanorian_pantheon
	holding = castle_holding

	# History
}
2530 = {

    # Misc
    holding = none

    # History

}

###c_marronath
851 = {		#Marronath

	# Misc
	culture = black_castanorian
	religion = castanorian_pantheon
	holding = castle_holding

	# History
}
2529 = {

    # Misc
    holding = none

    # History

}
2534 = {

    # Misc
    holding = church_holding

    # History

}

##d_steelhyl
###c_steelhyl
860 = {		#Steelhyl

	# Misc
	culture = castanorian
	religion = castanorian_pantheon
	holding = castle_holding

	# History
	1000.1.1 = {
		special_building_slot = steelhyl_mines_01
		special_building = steelhyl_mines_01
	}
}
2552 = {

    # Misc
    holding = city_holding

    # History

}
2553 = {

    # Misc
    holding = none

    # History

}
2554 = {

    # Misc
    holding = none

    # History

}

###c_coalwoud
862 = {		#Coalwoud

	# Misc
	culture = castanorian
	religion = castanorian_pantheon
	holding = castle_holding

	# History
}
2551 = {

    # Misc
    holding = none

    # History

}

##d_foarhal
###c_foarhal
2525 = {	#Foarhal

	# Misc
	culture = castanorian
	religion = castanorian_pantheon
	holding = castle_holding

	# History
}
853 = {

    # Misc
    holding = city_holding

    # History

}
2523 = {

    # Misc
    holding = castle_holding

    # History

}
2524 = {

    # Misc
    holding = none

    # History

}

###c_escerton
841 = {		#Escerton

	# Misc
	culture = castanorian
	religion = castanorian_pantheon
	holding = castle_holding

	# History
}
2521 = {

    # Misc
    holding = none

    # History

}
2522 = {

    # Misc
    holding = city_holding

    # History

}

###c_nathwoud
855 = {		#Nathwoud

	# Misc
	culture = castanorian
	religion = castanorian_pantheon
	holding = castle_holding

	# History
}
2526 = {

    # Misc
    holding = none

    # History

}
2527 = {

    # Misc
    holding = city_holding

    # History

}
2528 = {

    # Misc
    holding = none

    # History

}

##d_southgate
###c_southgate
830 = {		#Southgate

	# Misc
	culture = castanorian
	religion = castanorian_pantheon
	holding = castle_holding

	# History
	1000.1.1 = {
		special_building_slot = white_walls_of_castanor_01
		special_building = white_walls_of_castanor_01
	}
}
2518 = {

    # Misc
    holding = church_holding

    # History

}

###c_ar_urion
828 = {		#Ar Urion

	# Misc
	culture = castanorian
	religion = castanorian_pantheon
	holding = castle_holding

	# History
}
2519 = {

    # Misc
    holding = none

    # History

}
2520 = {

    # Misc
    holding = city_holding

    # History

}

###c_the_manorfields
2403 = {	#Silverdocks

    # Misc
    culture = castanorian
    religion = castanorian_pantheon
	holding = castle_holding

    # History

}
2516 = {

    # Misc
    holding = city_holding

    # History

}
2517 = {

    # Misc
    holding = church_holding

    # History

}
