#k_pearlsedge
##d_pearlsedge
###c_pearlsedge
43 = {		#Pearlsedge

    # Misc
    culture = pearlsedger
    religion = cult_of_the_dame #Unsure about these - just put as King Aron starts off as Castanorian Panteon right now
    holding = castle_holding

    # History

}
42 = {		#Erngrove

    # Misc
    holding = church_holding

    # History

}
1052 = {

    # Misc
	holding = castle_holding

    # History

}
1053 = {

    # Misc
	holding = none

    # History

}
943 = {

    # Misc
	holding = city_holding

    # History

}

###c_jewelpoint
46 = {		#Jewelpoint

    # Misc
    culture = pearlsedger
    religion = cult_of_the_dame
    holding = castle_holding

    # History
}
944 = {

    # Misc
	holding = city_holding

    # History

}

###c_the_pearls
14 = {		#The Pearls

    # Misc
    culture = pearlsedger
    religion = cult_of_the_dame
    holding = castle_holding

    # History
}
1056 = {

    # Misc
	holding = none

    # History

}
945 = {

    # Misc
	holding = none

    # History

}

##d_pearlywine
###c_lower_pearlywine
49 = { #Lower Pearlywine

    # Misc
    culture = pearlsedger
    religion = cult_of_the_dame
    holding = castle_holding

    # History
}
1054 = {

    # Misc
    holding = none

    # History

}
946 = {

    # Misc
    holding = city_holding

    # History

}
947 = {

    # Misc
    holding = church_holding

    # History

}

###c_upper_pearlywine
41 = {		#Upper Pearlywine

    # Misc
    culture = pearlsedger
    religion = cult_of_the_dame
    holding = castle_holding

    # History
}
1055 = {

    # Misc
    holding = city_holding

    # History

}
948 = {

    # Misc
    holding = none

    # History

}
949 = {

    # Misc
    holding = church_holding

    # History

}
