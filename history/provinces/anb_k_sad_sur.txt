#k_sad_sur
##d_sad_sur
###c_sad_sur
682 = {		#Sad Sur

    # Misc
    culture = sadnatu
    religion = bulwari_sun_cult
	holding = tribal_holding

    # History
}

###c_sihbaga
680 = {		#Sihbaga

    # Misc
    culture = sadnatu
    religion = bulwari_sun_cult
	holding = tribal_holding

    # History
}

###c_rakutmu
679 = {		#Rakutmu

    # Misc
    culture = sadnatu
    religion = bulwari_sun_cult
	holding = tribal_holding

    # History
}

##d_agshilsu
###c_arfajazan
597 = {		#Arfajazan

    # Misc
    culture = sadnatu
    religion = bulwari_sun_cult
	holding = tribal_holding

    # History
}

###c_umulu
685 = {		#Umulu

    # Misc
    culture = sadnatu
    religion = bulwari_sun_cult
	holding = tribal_holding

    # History
}

###c_elum_szel_bazi
683 = {		#Elum-szel-bazi

    # Misc
    culture = sadnatu
    religion = bulwari_sun_cult
	holding = tribal_holding

    # History
}

##d_ekluzagnu
###c_ekluzagnu
688 = {		#Ekluzagnu

    # Misc
    culture = sadnatu
    religion = bulwari_sun_cult
	holding = tribal_holding

    # History
	1000.1.1 = {
		special_building_slot = ekluzagnu_01
		special_building = ekluzagnu_01
	}
}

###c_sad_uras
689 = {		#Sad Uras

    # Misc
    culture = sadnatu
    religion = bulwari_sun_cult
	holding = tribal_holding

    # History
	1000.1.1 = {
		special_building_slot = sad_uras_mines_01
		special_building = sad_uras_mines_01
	}
}

##d_ardutibad
###c_anzakalis
687 = {		#Anzakalis

    # Misc
    culture = sadnatu
    religion = bulwari_sun_cult
	holding = tribal_holding

    # History
}

###c_harranbar
691 = {		#Harranbar

    # Misc
    culture = sadnatu
    religion = bulwari_sun_cult
	holding = tribal_holding

    # History
}

##d_tumerub
###c_kesudagka
693 = {		#Kesudagka

    # Misc
    culture = sadnatu
    religion = bulwari_sun_cult
	holding = tribal_holding

    # History
}

###c_mibunasru
686 = {		#Mibunasru

    # Misc
    culture = sadnatu
    religion = bulwari_sun_cult
	holding = tribal_holding

    # History
}

###c_ardubar
692 = {		#Ardubar

    # Misc
    culture = sadnatu
    religion = bulwari_sun_cult
	holding = tribal_holding

    # History
}

