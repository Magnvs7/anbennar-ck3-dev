#k_marrhold
##d_marrhold
###c_marrhold
4097 = {	#Marrhold

    # Misc
    culture = marrodic
    religion = castanorian_pantheon
	holding = castle_holding

    # History
}
2609 = {

    # Misc
    holding = city_holding

    # History

}
2610 = {

    # Misc
    holding = church_holding

    # History

}

###c_griffonsgate
895 = {		#Griffonsgate

	# Misc
	culture = marrodic
	religion = castanorian_pantheon
	holding = castle_holding

	# History
}
2601 = {

    # Misc
    holding = city_holding

    # History

}
2602 = {

    # Misc
    holding = none

    # History

}
2603 = {

    # Misc
    holding = church_holding

    # History

}

###c_marrvale
896 = {		#Marrvale

	# Misc
	culture = marrodic
	religion = castanorian_pantheon
	holding = castle_holding

	# History
}
2605 = {

    # Misc
    holding = none

    # History

}
2606 = {

    # Misc
    holding = none

    # History

}
2607 = {

    # Misc
    holding = city_holding

    # History

}

###c_marrhyl
897 = {		#Marrhyl

	# Misc
	culture = marrodic
	religion = castanorian_pantheon
	holding = castle_holding

	# History
}
2604 = {

    # Misc
    holding = none

    # History

}

##d_dryadsdale
###c_dryadsdale
891 = {		#Dryadsdale

	# Misc
	culture = marrodic
	religion = castanorian_pantheon
	holding = castle_holding

	# History
}
2590 = {

    # Misc
    holding = church_holding

    # History

}
2591 = {

    # Misc
    holding = none

    # History

}
2592 = {

    # Misc
    holding = none

    # History

}

###c_spritescage
883 = {		#Spritescage

	# Misc
	culture = marrodic
	religion = castanorian_pantheon
	holding = castle_holding

	# History
}
2585 = {

    # Misc
    holding = none

    # History

}
2586 = {

    # Misc
    holding = none

    # History

}
2587 = {

    # Misc
    holding = city_holding

    # History

}

###c_pixiebury
888 = {		#Pixiebury

	# Misc
	culture = marrodic
	religion = castanorian_pantheon
	holding = castle_holding

	# History
}
2588 = {

    # Misc
    holding = city_holding

    # History

}
2589 = {

    # Misc
    holding = church_holding

    # History

}

##d_doewood
###c_doescker
893 = {		#Doescker

	# Misc
	culture = marrodic
	religion = castanorian_pantheon
	holding = castle_holding

	# History
}
2597 = {

    # Misc
    holding = city_holding

    # History

}
2598 = {

    # Misc
    holding = none

    # History

}

###c_willowmore
894 = {		#Willowmore

	# Misc
	culture = marrodic
	religion = castanorian_pantheon
	holding = castle_holding

	# History
}
2599 = {

    # Misc
    holding = city_holding

    # History

}
2600 = {

    # Misc
    holding = none

    # History

}

##d_hornwood
###c_hornwood
889 = {		#Hornwood

	# Misc
	culture = marrodic
	religion = castanorian_pantheon
	holding = castle_holding

	# History
}
2594 = {

    # Misc
    holding = church_holding

    # History

}

###c_lovers_quarrel
892 = {		#Lovers' Quarrel

	# Misc
	culture = marrodic
	religion = castanorian_pantheon
	holding = castle_holding

	# History
}
2595 = {

    # Misc
    holding = city_holding

    # History

}

2596 = {

    # Misc
    holding = none

    # History

}

###c_nymphlea
890 = {		#Nymphlea

	# Misc
	culture = marrodic
	religion = castanorian_pantheon
	holding = castle_holding

	# History
}
2593 = {

    # Misc
    holding = none

    # History

}
