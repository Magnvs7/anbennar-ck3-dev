#k_damescrown
##d_damescrown
###c_damescrown
234 = {		#Damescrown

    # Misc
    culture = crownsman
    religion = cult_of_the_dame
	holding = city_holding

    # History
}
233 = {		#Ebenaire

    # Misc
    holding = city_holding

    # History
}
2035 = {

    # Misc
    holding = castle_holding

    # History

}
2036 = {

    # Misc
    holding = church_holding

    # History

}
2037 = {

    # Misc
    holding = none

    # History

}
2038 = {

    # Misc
    holding = none

    # History

}

###c_crothan
915 = {		#Crothan

    # Misc
    culture = crownsman
    religion = cult_of_the_dame
	holding = city_holding

    # History
}
2039 = {

    # Misc
    holding = none

    # History

}
2040 = {

    # Misc
    holding = church_holding

    # History

}
2041 = {

    # Misc
    holding = castle_holding

    # History

}

###c_aramar
249 = {		#Aramar

    # Misc
    culture = crownsman
    religion = cult_of_the_dame
	holding = church_holding

    # History
	1000.1.1 = {
		special_building_slot = aramar_mines_01
		special_building = aramar_mines_01
	}
}
2042 = {

    # Misc
    holding = city_holding

    # History

}
2043 = {

    # Misc
    holding = castle_holding

    # History

}
2044 = {

    # Misc
    holding = none

    # History

}

##d_aranmas
###c_aranmas
262 = {		#Aranmas

    # Misc
    culture = crownsman
    religion = cult_of_the_dame
	holding = castle_holding

    # History
}
1886 = {

    # Misc
    holding = church_holding

    # History

}
1887 = {

    # Misc
    holding = city_holding

    # History

}

###c_crownsway
255 = {		#Crownsway

    # Misc
    culture = crownsman
    religion = cult_of_the_dame
	holding = castle_holding

    # History
}
1888 = {

    # Misc
    holding = church_holding

    # History

}
1890 = {

    # Misc
    holding = none

    # History

}

###c_ainway
908 = {		#Ainway

    # Misc
    culture = crownsman
    religion = cult_of_the_dame

    # History
}
1892 = {

    # Misc
    holding = city_holding

    # History

}

###c_calascandar
254 = {		#Calascandar

    # Misc
    culture = crownsman
    religion = cult_of_the_dame
	holding = castle_holding

    # History
	1000.1.1 = {
		special_building_slot = calasandur_castle_calascandar_01
		special_building = calasandur_castle_calascandar_01
	}
}
1893 = {

    # Misc
    holding = city_holding

    # History

}
1901 = {

    # Misc
    holding = none

    # History

}
1904 = {

    # Misc
    holding = none

    # History

}

##d_crodam
###c_crodam
334 = {		#Crodam

    # Misc
    culture = gawedi
    religion = cult_of_the_dame
	holding = castle_holding

    # History
}
2084 = {

    # Misc
    holding = church_holding

    # History

}
2085 = {

    # Misc
    holding = none

    # History

}

###c_teagansfield
258 = {		#Teagansfield

    # Misc
    culture = crownsman
    religion = cult_of_the_dame
	holding = castle_holding

    # History
}
2051 = {

    # Misc
    holding = city_holding

    # History

}
2052 = {

    # Misc
    holding = none

    # History

}
2053 = {

    # Misc
    holding = none

    # History

}

###c_casnamoine
357 = {		#Casnamoine

    # Misc
    culture = moon_elvish
    religion = cult_of_the_dame
	holding = castle_holding

    # History
}
2054 = {

    # Misc
    holding = none

    # History

}

###c_canneinn
342 = {		#Canneinn

    # Misc
    culture = crownsman
    religion = cult_of_the_dame
	holding = castle_holding

    # History
}
2050 = {

    # Misc
    holding = church_holding

    # History

}

###c_derwing
253 = {		#Derwing

    # Misc
    culture = gawedi
    religion = cult_of_the_dame
	holding = castle_holding

    # History
}
2081 = {

    # Misc
    holding = church_holding

    # History

}
2082 = {

    # Misc
    holding = none

    # History

}
2083 = {

    # Misc
    holding = city_holding

    # History

}

###c_wrothcross
236 = {		#Wrothcross

    # Misc
    culture = gawedi
    religion = cult_of_the_dame
	holding = castle_holding

    # History
}
2078 = {

    # Misc
    holding = city_holding

    # History

}
2079 = {

    # Misc
    holding = none

    # History

}
2080 = {

    # Misc
    holding = none

    # History

}

###c_golden_plains
250 = {		#Golden Plains

    # Misc
    culture = gawedi
    religion = cult_of_the_dame
	holding = castle_holding

    # History
}
2086 = {

    # Misc
    holding = city_holding

    # History

}
2087 = {

    # Misc
    holding = none

    # History

}

###c_elfsmithy
335 = {		#Elfsmithy

    # Misc
    culture = moon_elvish
    religion = cult_of_the_dame
	holding = castle_holding

    # History
}
2045 = {

    # Misc
    holding = city_holding

    # History

}
2046 = {

    # Misc
    holding = none

    # History

}

###c_marcestir
914 = {		#Marcestir

    # Misc
    culture = crownsman
    religion = cult_of_the_dame
	holding = castle_holding

    # History
}
2047 = {

    # Misc
    holding = city_holding

    # History

}
2048 = {

    # Misc
    holding = none

    # History

}
2049 = {

    # Misc
    holding = none

    # History

}
