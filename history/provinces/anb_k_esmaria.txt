#k_esmaria
##d_estallen
###c_estallen
280 = {		#Estallen

    # Misc
    culture = ryalani
    religion = cult_of_the_dame
	holding = castle_holding

    # History
}
1861 = {	

    # Misc
    holding = none

    # History

}
1862 = {

    # Misc
    holding = city_holding

    # History

}
1863 = {

    # Misc
    holding = church_holding

    # History

}

###c_hatters_green
297 = {		#Hatter's Green

    # Misc
    culture = ryalani
    religion = cult_of_the_dame
	holding = castle_holding

    # History
}
1864 = {

    # Misc
    holding = church_holding

    # History

}

###c_tomansford
44 = {		#Tomansford

    # Misc
    culture = ryalani
    religion = cult_of_the_dame
	holding = castle_holding

    # History
}
1865 = {

    # Misc
    holding = church_holding

    # History

}
1866 = {

    # Misc
    holding = city_holding

    # History

}

###c_pinklady
576 = { #Pinklady Pier

    # Misc
    culture = ryalani
    religion = cult_of_the_dame
	holding = castle_holding

    # History
}
1867 = {

    # Misc
    holding = city_holding

    # History

}
1868 = {

    # Misc
    holding = none

    # History

}

###c_terr
301 = {		#Terr

    # Misc
    culture = ryalani
    religion = cult_of_the_dame

    # History
}
1869 = {

    # Misc
    holding = church_holding

    # History

}
1870 = {

    # Misc
    holding = none

    # History

}

##d_konwell
###c_konwell
275 = {		#Konwell

    # Misc
    culture = esmari
    religion = cult_of_the_dame
	holding = castle_holding

    # History
}
1841 = {

    # Misc
    holding = none

    # History

}
1842 = {

    # Misc
    holding = city_holding

    # History

}
1843 = {

    # Misc
    holding = church_holding

    # History

}

###c_millsbay
905 = {		#Millsbay

    # Misc
    culture = esmari
    religion = cult_of_the_dame
	holding = castle_holding

    # History
}
1844 = {

    # Misc
    holding = none

    # History

}
1845 = {

    # Misc
    holding = city_holding

    # History

}

###c_fencewood
269 = {		#Fencewood

    # Misc
    culture = esmari
    religion = cult_of_the_dame
	holding = castle_holding

    # History
}
1846 = {

    # Misc
    holding = church_holding

    # History

}
1847 = {

    # Misc
    holding = none

    # History

}

##d_bennon
###c_bennon
268 = {		#Bennon

    # Misc
    culture = esmari
    religion = cult_of_the_dame
	holding = castle_holding

    # History
}
1907 = {

    # Misc
    holding = city_holding

    # History

}
1909 = {

    # Misc
    holding = church_holding

    # History

}

###c_bennonhill
270 = {		#Bennonhill

    # Misc
    culture = esmari
    religion = cult_of_the_dame
	holding = castle_holding

    # History
}
1910 = {

    # Misc
    holding = city_holding

    # History

}
1911 = {

    # Misc
    holding = none

    # History

}

###c_oldpassage
263 = {		#Oldpassage

    # Misc
    culture = esmari
    religion = cult_of_the_dame
	holding = castle_holding

    # History
	1000.1.1 = {
		special_building_slot = oldpassage_01
		special_building = oldpassage_01
	}
}
1905 = {

    # Misc
    holding = city_holding

    # History

}
1906 = {

    # Misc
    holding = none

    # History

}

###c_giberd
271 = {		#Giberd

    # Misc
    culture = cliff_gnomish
    religion = cult_of_the_dame
	holding = city_holding

    # History
}
1912 = {

    # Misc
    holding = castle_holding

    # History

}

###c_havorton
909 = {		#Havorton

    # Misc
    culture = esmari
    religion = cult_of_the_dame
	holding = castle_holding

    # History
}
1913 = {

    # Misc
    holding = city_holding

    # History

}
1914 = {

    # Misc
    holding = none

    # History

}

##d_leslinpar
###c_leslinpar
901 = {		#Leslinpar

    # Misc
    culture = esmari
    religion = cult_of_the_dame
	holding = castle_holding

    # History
}
1915 = {

    # Misc
    holding = none

    # History

}
1916 = {

    # Misc
    holding = city_holding

    # History

}
1917 = {

    # Misc
    holding = church_holding

    # History

}

###c_varaine
309 = {		#Varaine

    # Misc
    culture = esmari
    religion = cult_of_the_dame
	holding = castle_holding

    # History
}
1918 = {

    # Misc
    holding = city_holding

    # History

}
1925 = {

    # Misc
    holding = none

    # History

}

###c_yellowford
302 = {		#Yellowford

    # Misc
    culture = esmari
    religion = cult_of_the_dame
	holding = castle_holding

    # History
}
1930 = {

    # Misc
    holding = church_holding

    # History

}

##d_cann_esmar
###c_esmaraine
266 = {

    # Misc
    culture = esmari
    religion = cult_of_the_dame
	holding = castle_holding

    # History
}
1874 = {

    # Misc
    holding = city_holding

    # History

}
1875 = {

    # Misc
    holding = church_holding

    # History

}
1876 = {

    # Misc
    holding = castle_holding

    # History

}
1877 = {

    # Misc
    holding = none

    # History

}

###c_coastroad
259 = {		#Coastroad

    # Misc
    culture = esmari
    religion = cult_of_the_dame
	holding = castle_holding

    # History
}
1882 = {

    # Misc
    holding = city_holding

    # History

}
1883 = {

    # Misc
    holding = none

    # History

}
1884 = {

    # Misc
    holding = none

    # History

}

###c_telgeir
911 = {		#Telgeir

    # Misc
    culture = esmari
    religion = cult_of_the_dame
	holding = castle_holding

    # History
}
1881 = {

    # Misc
    holding = city_holding

    # History

}

###c_gabmorionn
260 = {		#Gabmorionn

    # Misc
    culture = esmari
    religion = cult_of_the_dame
	holding = castle_holding

    # History
}
1878 = {

    # Misc
    holding = none

    # History

}
1879 = {

    # Misc
    holding = none

    # History

}
1880 = {

    # Misc
    holding = city_holding

    # History

}

###c_themarenn
267 = {		#Themarenn

    # Misc
    culture = esmari
    religion = cult_of_the_dame
	holding = castle_holding

    # History
}
1871 = {

    # Misc
    holding = city_holding

    # History

}
1872 = {

    # Misc
    holding = church_holding

    # History

}
1873 = {

    # Misc
    holding = none

    # History

}

##d_ryalanar
###c_ryalanar
265 = {		#Ryalanar

    # Misc
    culture = ryalani
    religion = cult_of_the_dame
	holding = church_holding

    # History
}
1850 = {

    # Misc
    holding = city_holding

    # History

}
1851 = {

    # Misc
    holding = none

    # History

}

###c_yearncliff
264 = {		#Yearncliff

    # Misc
    culture = ryalani
    religion = cult_of_the_dame
	holding = castle_holding

    # History
}
1852 = {

    # Misc
    holding = none

    # History

}

###c_loveswood
917 = {		#Loveswood

    # Misc
    culture = ryalani
    religion = cult_of_the_dame
	holding = castle_holding

    # History
}
575 = {		#Sweetcastle

    # Misc
    holding = castle_holding

    # History
}
1848 = {

    # Misc
    holding = none

    # History

}
1849 = {

    # Misc
    holding = none

    # History

}

##d_songbarges
###c_seinathil
916 = {		#Seinathil

    # Misc
    culture = ryalani
    religion = cult_of_the_dame
	holding = castle_holding

    # History
}
1857 = {

    # Misc
    holding = church_holding

    # History

}

###c_esterfield
279 = {		#Esterfield

    # Misc
    culture = ryalani
    religion = cult_of_the_dame
	holding = castle_holding

    # History
}
1853 = {

    # Misc
    holding = city_holding

    # History

}
1854 = {

    # Misc
    holding = church_holding

    # History

}
1855 = {

    # Misc
    holding = none

    # History

}
1856 = {

    # Misc
    holding = none

    # History

}

###c_notesbridge
274 = {		#Notesbridge

    # Misc
    culture = ryalani
    religion = cult_of_the_dame
	holding = castle_holding

    # History
}
1858 = {

    # Misc
    holding = city_holding

    # History

}
1859 = {

    # Misc
    holding = none

    # History

}

###c_deamoine
333 = {		#Deamoine

    # Misc
    culture = ryalani
    religion = cult_of_the_dame
	holding = castle_holding

    # History
}
1860 = {

    # Misc
    holding = city_holding

    # History

}

##d_asheniande
###c_ashfields
311 = {		#Ashfields

    # Misc
    culture = esmari
    religion = cult_of_the_dame
	holding = castle_holding

    # History
}
1944 = {

    # Misc
    holding = church_holding

    # History

}
1945 = {

    # Misc
    holding = none

    # History

}
1946 = {

    # Misc
    holding = city_holding

    # History

}

###c_teinmas
314 = {		#Teinmass

    # Misc
    culture = ryalani
    religion = cult_of_the_dame
	holding = castle_holding

    # History
}
1722 = {

    # Misc
    holding = church_holding

    # History

}
1723 = {

    # Misc
    holding = none

    # History

}
1724 = {

    # Misc
    holding = none

    # History

}

###c_embergarden
902 = {		#Embergarden

    # Misc
    culture = esmari
    religion = cult_of_the_dame
	holding = castle_holding

    # History
}
1940 = {

    # Misc
    holding = none

    # History

}

1941 = {

    # Misc
    holding = city_holding

    # History

}

###c_cestirbridge
310 = {		#Cestirbridge

    # Misc
    culture = esmari
    religion = cult_of_the_dame
	holding = castle_holding

    # History
}
1942 = {

    # Misc
    holding = city_holding

    # History

}
1943 = {

    # Misc
    holding = none

    # History

}

###c_livergrave
320 = {

    # Misc
    culture = esmari
    religion = cult_of_the_dame
	holding = castle_holding

    # History
}
1947 = {

    # Misc
    holding = church_holding

    # History

}

##d_hearthswood
###c_hearthswood
272 = {		#Hearthswood

    # Misc
    culture = esmari
    religion = cult_of_the_dame
	holding = castle_holding

    # History
}
1933 = {

    # Misc
    holding = church_holding

    # History

}
1934 = {

    # Misc
    holding = none

    # History

}

###c_elvelenn
900 = {		#Elvelenn

    # Misc
    culture = moon_elvish
    religion = cult_of_the_dame
	holding = castle_holding

    # History
}
1935 = {

    # Misc
    holding = city_holding

    # History

}
1936 = {

    # Misc
    holding = none

    # History

}

###c_tendergrove
273 = {		#Tendergrove

    # Misc
    culture = esmari
    religion = cult_of_the_dame
	holding = castle_holding

    # History
}
1931 = {

    # Misc
    holding = none

    # History

}

##d_silverforge
###c_anvilwright
912 = {		#Anvilwright
	#Make this not built. Ideally this would be a dwarven hold type holding that dwarves can build in the future. First holding in slot must start built though. 
	
    # Misc
    culture = silver_dwarvish
    religion = cult_of_the_dame
	holding = castle_holding

    # History
}
308 = {		#Silverforge Hall

    # Misc
	holding = city_holding

    # History
}
1937 = {

    # Misc
    holding = none

    # History

}

1938 = {

    # Misc
    holding = none

    # History

}

1939 = {

    # Misc
    holding = none

    # History

}
