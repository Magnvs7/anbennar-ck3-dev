﻿# Anbennar - disabled generate family by default and removed mongol and hungarian invasions

racial_legitimacy_and_supremacy = {
	default = on_racism

	legitimacy_and_supremacy_on = {

	}
	
	legitimacy_and_supremacy_off = {
	}
}

marked_by_destiny = {
	default = enable_marked_by_destiny

	enable_marked_by_destiny = {

	}
	
	disable_marked_by_destiny = {
	}
}