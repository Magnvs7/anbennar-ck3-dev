﻿language_common = {
	type = language
	is_shown = {
		language_is_shown_trigger = {
			LANGUAGE = language_common
		}
	}
	ai_will_do = {
		value = 10
		if = {
			limit = { has_cultural_pillar = language_common }
			multiply = 10
		}
	}
	
	color = { 222 222 222 }
}

language_bulwari = {
	type = language
	is_shown = {
		language_is_shown_trigger = {
			LANGUAGE = language_bulwari
		}
	}
	ai_will_do = {
		value = 10
		if = {
			limit = { has_cultural_pillar = language_bulwari }
			multiply = 10
		}
	}
	
	color = { 222 222 0 }
}

language_kheteratan = {
	type = language
	is_shown = {
		language_is_shown_trigger = {
			LANGUAGE = language_kheteratan
		}
	}
	ai_will_do = {
		value = 10
		if = {
			limit = { has_cultural_pillar = language_kheteratan }
			multiply = 10
		}
	}
	
	color = { 255 128 14 }
}

#a kheteratan dialect
language_akani = {
	type = language
	is_shown = {
		language_is_shown_trigger = {
			LANGUAGE = language_akani
		}
	}
	ai_will_do = {
		value = 10
		if = {
			limit = { has_cultural_pillar = language_akani }
			multiply = 10
		}
	}
	
	color = { 112 146 190 }
}

#a kheteratan dialect
language_akani = {
	type = language
	is_shown = {
		language_is_shown_trigger = {
			LANGUAGE = language_akani
		}
	}
	ai_will_do = {
		value = 10
		if = {
			limit = { has_cultural_pillar = language_akani }
			multiply = 10
		}
	}
	
	color = { 221 204 164 }
}

language_gerudian = {
	type = language
	is_shown = {
		language_is_shown_trigger = {
			LANGUAGE = language_gerudian
		}
	}
	ai_will_do = {
		value = 10
		if = {
			limit = { has_cultural_pillar = language_gerudian }
			multiply = 10
		}
	}
	
	color = { 163 211 222 }
}

language_dwarven = {
	type = language
	is_shown = {
		language_is_shown_trigger = {
			LANGUAGE = language_dwarven
		}
	}
	ai_will_do = {
		value = 10
		if = {
			limit = { has_cultural_pillar = language_dwarven }
			multiply = 10
		}
	}
	
	color = { 128 128 128 }
}

#Should elven be divided? for now its not
language_elven = {
	type = language
	is_shown = {
		language_is_shown_trigger = {
			LANGUAGE = language_elven
		}
	}
	ai_will_do = {
		value = 10
		if = {
			limit = { has_cultural_pillar = language_elven }
			multiply = 10
		}
	}
	
	color = { 128 240 240 }
}

language_old_castanorian = {
	type = language
	is_shown = {
		language_is_shown_trigger = {
			LANGUAGE = language_old_castanorian
		}
	}
	ai_will_do = {
		value = 10
		if = {
			limit = { has_cultural_pillar = language_old_castanorian }
			multiply = 10
		}
	}
	
	color = { 255 255 255 }
}
