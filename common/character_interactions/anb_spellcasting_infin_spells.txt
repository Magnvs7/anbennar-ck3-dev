﻿### Hostile Spells ###

# Compel - Gain a weak hook #

start_compel_interaction = {
	
	category = interaction_category_spells
	scheme = anb_compel_spell

	is_shown = {
		#has_perk = _perk # Interaction unlocked by Perk
		scope:actor = { has_magical_affinity = yes }
		NOT = { scope:recipient = scope:actor }
	}
	
	send_name = cast_compel_start

	is_valid_showing_failures_only = {
		scope:actor = {
			can_start_scheme = {
				type = anb_compel_spell
				target = scope:recipient
			}
		}
	}

	on_accept = {
		scope:actor = {
			stress_impact = {
				compassionate = major_stress_impact_gain
				honest = medium_stress_impact_gain
				just = medium_stress_impact_gain
			}
		}
		hidden_effect = {
			scope:actor = {
				send_interface_toast = {
					title = start_compel_interaction_notification

					left_icon = scope:actor					
					right_icon = scope:recipient

					start_scheme = {
						type = anb_compel_spell
						target = scope:recipient
					}

					show_as_tooltip = {
						stress_impact = {
							compassionate = major_stress_impact_gain
							honest = medium_stress_impact_gain
							just = medium_stress_impact_gain
						}
					}
				}
			}
		}
	}
	
	auto_accept = yes

}

# Dominate - Gain a strong hook #

start_dominate_interaction = {
	
	category = interaction_category_spells
	scheme = anb_dominate_spell

	is_shown = {
		#has_perk = _perk # Interaction unlocked by Perk
		scope:actor = { has_magical_affinity = yes }
		NOT = { scope:recipient = scope:actor }
	}
	
	send_name = cast_dominate_start

	is_valid_showing_failures_only = {
		scope:actor = {
			can_start_scheme = {
				type = anb_dominate_spell
				target = scope:recipient
			}
		}
	}

	on_accept = {
		scope:actor = {
			stress_impact = {
				compassionate = major_stress_impact_gain
				honest = medium_stress_impact_gain
				just = medium_stress_impact_gain
			}
		}
		hidden_effect = {
			scope:actor = {
				send_interface_toast = {
					title = start_dominate_interaction_notification

					left_icon = scope:actor					
					right_icon = scope:recipient

					start_scheme = {
						type = anb_dominate_spell
						target = scope:recipient
					}

					show_as_tooltip = {
						stress_impact = {
							compassionate = major_stress_impact_gain
							honest = medium_stress_impact_gain
							just = medium_stress_impact_gain
						}
					}
				}
			}
		}
	}
	
	auto_accept = yes

}