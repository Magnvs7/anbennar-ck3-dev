﻿#Roilsard
house_roilsard = {	#sil Roilsard
	prefix = "dynnp_sil"
	name = "dynn_Roilsard"
	dynasty = dynasty_roilsardis	#Roilsarding
}

house_vivin = {	#sil Vivin
	prefix = "dynnp_sil"
	name = "dynn_Vivin"
	dynasty = dynasty_roilsardis
}
house_loop = {	#silna Loop
	prefix = "dynnp_sil_na"
	name = "dynn_Loop"
	dynasty = dynasty_roilsardis	#Roilsarding
}

house_saloren = {	#sil Saloren
	prefix = "dynnp_sil"
	name = "dynn_Saloren"
	dynasty = dynasty_roilsardis	#Roilsarding
}

house_dameris = { #Dameris
	name = "dynn_Dameris"
	dynasty = 14	#Lorentis - actually from Rubentis line tho
}

house_lorentis = { #Lorentis
	name = "dynn_Lorentis"
	dynasty = 14	#Lorentis
}

house_caylentis = { #Caylentis
	name = "dynn_Caylentis"
	dynasty = 14	#Lorentis
}

house_rubentis = { #Rubentis
	name = "dynn_Rubentis"
	dynasty = 14	#Lorentis
}

house_sil_kyliande = {
	prefix = "dynnp_sil"
	name = "dynn_Kyliande"
	motto = dynn_sil_Kyliande_motto
	dynasty = dynasty_caylentis
}

house_caylenoris = {
	name = "dynn_Caylenoris"
	dynasty = dynasty_caylentis
}

house_rewantis = { #Rewantis
	name = "dynn_Rewantis"
	dynasty = 14	#Lorentis
	motto = dynn_Rewantis_motto
}

house_ironbark = { #Ironbark
	name = "dynn_Ironbark"
	dynasty = 14	#Lorentis
}

house_appleseed = { #Appleseed
	name = "dynn_Appleseed"
	dynasty = 19 	#Roysfort
}

house_peartree = { #Peartree
	name = "dynn_Peartree"
	dynasty = 19 	#Roysfort
}

house_coldsteel = { #Coldsteel
	name = "dynn_Coldsteel"
	dynasty = dynasty_gawe	#Gawe
}

house_beron = { #Beron
	name = "dynn_Beron"
	dynasty = 26	#Mooring
}

house_fouler = { #Fouler
	name = "dynn_Fouler"
	dynasty = 26 	#Mooring
}

house_cottersea = { #Cottersea
	name = "dynn_Cottersea"
	dynasty = 26	#Mooring
}

house_wight = { #Wight
	name = "dynn_Wight"
	dynasty = 26	#Mooring
	motto = dynn_Wight_motto
}

house_alcarsson = { #Alcarsson
	name = "dynn_Alcarsson"
	dynasty = 45 #Vrorensson
}

house_lanpool = {
	prefix = "dynnp_of"
	name = "dynn_Lanpool"
	dynasty = dynasty_tretunis
}

house_vanbury = {
	prefix = "dynnp_of"
	name = "dynn_Vanbury"
	dynasty = dynasty_gawe
}

house_vanbury_steelhyl = {
	prefix = "dynnp_of"
	name = "dynn_Vanbury_Steelhyl"
	dynasty = dynasty_gawe
}

house_bluetongue = {
	name = "dynn_Bluetongue"
	dynasty = dynasty_cymlan
}

house_edharlain = {
	name = "dynn_Edharlain"
	dynasty = 57 #Esshyl
	motto = dynn_Edharlain_motto
}

house_alencay = {
	name = "dynn_alencay"
	dynasty = dynasty_sigvardsson
}

house_terr = {
	name = "dynn_Terr"
	dynasty = dynasty_ottocam
}