﻿#What the victim thinks - change values depending on spell
attempted_forbidden_magic_opinion = {
	opinion = -30
	stacking = yes
	decaying = yes
	years = 30
}

#Everyone's opinion towards people with the trait and criminal doctrine
forbidden_magic_crime = {
	opinion = -20
	imprisonment_reason = yes
	banish_reason = yes
	execute_reason = yes
}

#Everyone's opinion towards people with the trait and shunned doctrine
forbidden_magic_intolerant = {
	opinion = -20
}