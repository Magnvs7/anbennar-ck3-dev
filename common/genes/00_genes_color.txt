﻿color_genes = {
	hair_color = {
		#index = 0 - vanilla removed this. not sure if we added this for Anbennar
		group = hair
		color = hair
		blend_range = { 0.0 0.0 }
	}
	skin_color = {
		sync_inheritance_with = hair_color
		#index = 1
		group = body
		color = skin
		blend_range = { 0.55 0.65 }
	}
	eye_color = {
		sync_inheritance_with = hair_color
		#index = 2
		group = eyes
		color = eye
		blend_range = { 0.0 0.0 }
	}

}