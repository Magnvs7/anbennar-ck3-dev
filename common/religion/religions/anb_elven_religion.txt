﻿elven_religion = {
	family = rf_elven
	graphical_faith = dharmic_gfx
	doctrine = elven_hostility_doctrine 

	#Main Group
	doctrine = doctrine_no_head	
	doctrine = doctrine_gender_equal
	doctrine = doctrine_pluralism_pluralistic
	doctrine = doctrine_theocracy_temporal

	#Marriage
	doctrine = doctrine_monogamy
	doctrine = doctrine_divorce_disallowed
	doctrine = doctrine_bastardry_none
	doctrine = doctrine_consanguinity_aunt_nephew_and_uncle_niece

	#Crimes
	doctrine = doctrine_homosexuality_crime
	doctrine = doctrine_adultery_men_crime
	doctrine = doctrine_adultery_women_crime
	doctrine = doctrine_kinslaying_any_dynasty_member_crime
	doctrine = doctrine_deviancy_crime
	doctrine = doctrine_witchcraft_crime

	#Clerical Functions
	doctrine = doctrine_clerical_function_alms_and_pacification
	doctrine = doctrine_clerical_gender_either
	doctrine = doctrine_clerical_marriage_allowed
	doctrine = doctrine_clerical_succession_temporal_appointment
	
	traits = {	#standard castanorian
		virtues = {
			diligent
			just
			honest
		}
		sins = {
			lazy
			greedy
			arbitrary
		}
	}

	custom_faith_icons = {	#TODO
		custom_faith_1 custom_faith_2 custom_faith_3 custom_faith_4 custom_faith_5 custom_faith_6 custom_faith_7 custom_faith_8 custom_faith_9 custom_faith_10 dualism_custom_1 zoroastrian_custom_1 zoroastrian_custom_2 buddhism_custom_1 buddhism_custom_2 buddhism_custom_3 buddhism_custom_4 taoism_custom_1 yazidi_custom_1 sunni_custom_2 sunni_custom_3 sunni_custom_4 ibadi_custom muhakkima_1 muhakkima_2 muhakkima_4 muhakkima_5 muhakkima_6 judaism_custom_1
	}

	holy_order_names = {	#TODO
		{ name = "holy_order_followers_of_arjuna" }
		{ name = "holy_order_faith_maharatas" }
		{ name = "holy_order_vyuha_of_highgod" }
		{ name = "holy_order_vyuha_of_the_temple_of_place" }
		{ name = "holy_order_maharatas_of_highgod" }
	}

	holy_order_maa = { praetorian }	#their men at arms

	localization = {
		#HighGod - Castellos
		HighGodName = elven_high_god_name
		HighGodNamePossessive = elven_high_god_name_possessive
		HighGodNameSheHe = CHARACTER_SHEHE_IT
		HighGodHerselfHimself = CHARACTER_ITSELF
		HighGodHerHis = CHARACTER_HERHIS_ITS
		HighGodNameAlternate = elven_high_god_name_alternate
		HighGodNameAlternatePossessive = elven_high_god_name_alternate_possessive

		#Creator
		CreatorName = elven_creator_god_name
		CreatorNamePossessive = elven_creator_god_name_possessive
		CreatorSheHe = CHARACTER_SHEHE_HE
		CreatorHerHis = CHARACTER_HERHIS_HIS
		CreatorHerHim = CHARACTER_HERHIM_HIM

		#HealthGod
		HealthGodName = elven_health_god_name
		HealthGodNamePossessive = elven_health_god_name_possessive
		HealthGodSheHe = CHARACTER_SHEHE_SHE
		HealthGodHerHis = CHARACTER_HERHIS_HER
		HealthGodHerHim = CHARACTER_HERHIM_HER
		
		#FertilityGod
		FertilityGodName = elven_fertility_god_name
		FertilityGodNamePossessive = elven_fertility_god_name_possessive
		FertilityGodSheHe = CHARACTER_SHEHE_SHE
		FertilityGodHerHis = CHARACTER_HERHIS_HER
		FertilityGodHerHim = CHARACTER_HERHIM_HER

		#WealthGod
		WealthGodName = elven_wealth_god_name
		WealthGodNamePossessive = elven_wealth_god_name_possessive
		WealthGodSheHe = CHARACTER_SHEHE_HE
		WealthGodHerHis = CHARACTER_HERHIS_HIS
		WealthGodHerHim = CHARACTER_HERHIM_HIM

		#HouseholdGod
		HouseholdGodName = elven_household_god_name
		HouseholdGodNamePossessive = elven_household_god_name_possessive
		HouseholdGodSheHe = CHARACTER_SHEHE_SHE
		HouseholdGodHerHis = CHARACTER_HERHIS_HER
		HouseholdGodHerHim = CHARACTER_HERHIM_HER

		#FateGod
		FateGodName = elven_fate_god_name
		FateGodNamePossessive = elven_fate_god_name_possessive
		FateGodSheHe = CHARACTER_SHEHE_IT
		FateGodHerHis = CHARACTER_HERHIS_ITS
		FateGodHerHim = CHARACTER_HERHIM_IT

		#KnowledgeGod
		KnowledgeGodName = elven_knowledge_god_name
		KnowledgeGodNamePossessive = elven_knowledge_god_name_possessive
		KnowledgeGodSheHe = CHARACTER_SHEHE_SHE
		KnowledgeGodHerHis = CHARACTER_HERHIS_HER
		KnowledgeGodHerHim = CHARACTER_HERHIM_HER

		#WarGod
		WarGodName = elven_war_god_name
		WarGodNamePossessive = elven_war_god_name_possessive
		WarGodSheHe = CHARACTER_SHEHE_SHE
		WarGodHerHis = CHARACTER_HERHIS_HER
		WarGodHerHim = CHARACTER_HERHIM_HER

		#TricksterGod
		TricksterGodName = elven_trickster_god_name
		TricksterGodNamePossessive = elven_trickster_god_name_possessive
		TricksterGodSheHe = CHARACTER_SHEHE_HE
		TricksterGodHerHis = CHARACTER_HERHIS_HIS
		TricksterGodHerHim = CHARACTER_HERHIM_HIM

		#NightGod
		NightGodName = elven_night_god_name
		NightGodNamePossessive = elven_night_god_name_possessive
		NightGodSheHe = CHARACTER_SHEHE_SHE
		NightGodHerHis = CHARACTER_HERHIS_HER
		NightGodHerHim = CHARACTER_HERHIM_HER

		#WaterGod
		WaterGodName = elven_water_god_name
		WaterGodNamePossessive = elven_water_god_name_possessive
		WaterGodSheHe = CHARACTER_SHEHE_SHE
		WaterGodHerHis = CHARACTER_HERHIS_HER
		WaterGodHerHim = CHARACTER_HERHIM_HER


		PantheonTerm = religion_the_gods
		PantheonTermHasHave = pantheon_term_have
		GoodGodNames = {
			elven_high_god_name
		}
		
		
		DevilName = elven_devil_name
		DevilNamePossessive = elven_devil_name_possessive
		DevilSheHe = CHARACTER_SHEHE_THEY
		DevilHerHis = CHARACTER_HERHIS_THEIR
		DevilHerselfHimself = elven_devil_herselfhimself
		EvilGodNames = {
			elven_devil_name
		}
		HouseOfWorship = elven_house_of_worship
		HouseOfWorshipPlural = elven_house_of_worship_plural
		ReligiousSymbol = elven_religious_symbol
		ReligiousText = elven_religious_text
		ReligiousHeadName = elven_religious_head_title
		ReligiousHeadTitleName = elven_religious_head_title_name
		DevoteeMale = elven_devotee_male
		DevoteeMalePlural = elven_devotee_male_plural
		DevoteeFemale = elven_devotee_female
		DevoteeFemalePlural = elven_devotee_female_plural
		DevoteeNeuter = elven_devotee_neuter
		DevoteeNeuterPlural = elven_devotee_neuter_plural
		PriestMale = elven_priest
		PriestMalePlural = elven_priest_plural
		PriestFemale = elven_priest
		PriestFemalePlural = elven_priest_plural
		PriestNeuter = elven_priest
		PriestNeuterPlural = elven_priest_plural
		AltPriestTermPlural = elven_priest_term_plural
		BishopMale = elven_bishop
		BishopMalePlural = elven_bishop_plural
		BishopFemale = elven_bishop
		BishopFemalePlural = elven_bishop_plural
		BishopNeuter = elven_bishop
		BishopNeuterPlural = elven_bishop_plural
		DivineRealm = elven_divine_realm
		PositiveAfterLife = elven_positive_afterlife
		NegativeAfterLife = elven_negative_afterlife
		DeathDeityName = elven_death_name
		DeathDeityNamePossessive = elven_death_name_possessive
		DeathDeitySheHe = CHARACTER_SHEHE_HE
		DeathDeityHerHis = CHARACTER_HERHIS_HIS
		WitchGodName = shaktism_good_god_the_dame
		WitchGodHerHis = CHARACTER_HERHIS_HER
		WitchGodSheHe = CHARACTER_SHEHE_SHE
		WitchGodHerHim = CHARACTER_HERHIM_HER
		WitchGodMistressMaster = mistress
		WitchGodMotherFather = mother

		GHWName = ghw_purification
		GHWNamePlural = ghw_purifications
	}

	faiths = {
		elven_forebears = {
			color = { 153 217 234 }
			icon = elven_forebears

			holy_site = anbenncost 
			# holy_site = moonmount
			# holy_site = mathura
			# holy_site = haridwar
			# holy_site = kanchipuram
			# holy_site = ujjayini
			# holy_site = dwarka

			doctrine = tenet_ancestor_worship	
			doctrine = tenet_astrology
			doctrine = tenet_communal_identity	
		}
	}
}
