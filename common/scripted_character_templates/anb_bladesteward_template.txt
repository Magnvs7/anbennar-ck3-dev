﻿bladesteward_character = {
	age = { 20 50 }
	trait = order_member
	#Education
	random_traits_list = {
		count = 1
		education_martial_1 = {
			weight = { base = 5 }
		}
		education_martial_2 = {
			weight = { base = 10 }
		}
		education_martial_3 = {
			weight = { base = 20 }
		}
		education_martial_4 = {
			weight = { base = 10 }
		}
	}
	#Personality
	random_traits_list = {
		count = 3
		ambitious = { 
			weight = { base = 5 } 
		}
		arrogant = { 
			weight = { base = 5 } 
		}
		just = { 
			weight = { base = 10 } 
		}
		zealous = { 
			weight = { base = 5 } 
		}
		brave = { 
			weight = { base = 10 } 
		}
		diligent = { 
			weight = { base = 5 } 
		}
		wrathful = { 
			weight = { base = 5 } 
		}
		humble = { 
			weight = { base = 5 }
		}
		stubborn = { 
			weight = { base = 5 } 
		}
	}
	#Commander
	random_traits_list = {
		count = 1
		unyielding_defender = {
			weight = { base = 5 }
		}
		forder = {
			weight = { base = 5 }
		}
		logistician = {
			weight = { base = 10 }
		}
		military_engineer = {
			weight = { base = 10 }
		}
		organizer = {
			weight = { base = 10 }
		}
		rough_terrain_expert = {
			weight = { base = 20 }
		}
		aggressive_attacker = {
			weight = { base = 50 }
		}
		reckless = {
			weight = { base = 20 }
		}
		open_terrain_expert = {
			weight = { base = 20 }
		}
		desert_warrior = {
			trigger = {
				root = {
					any_sub_realm_barony = {
						title_province = {
							terrain = desert
						}
					}
				}
			}
			weight = { base = 50 }
		}
		jungle_stalker = {
			trigger = {
				root = {
					any_sub_realm_barony = {
						title_province = {
							terrain = jungle
						}
					}
				}
			}
			weight = { base = 50 }
		}
		holy_warrior = {
			weight = { base = 20 }
		}
	}
	faith = root.faith
	culture = root.culture
	gender_female_chance = root_faith_clergy_gender_female_chance
	martial = {
		min_template_high_skill
		max_template_high_skill
	}
	prowess = {
		min_template_high_skill
		max_template_high_skill
	}
	#Anbennar
	after_creation = {
		if = {
			limit = {
				NOT = { has_racial_trait = yes }
			}
			assign_racial_trait_effect = yes
		}
	}
}
