﻿magic_mastery_icon = {
	is_shown = {
		has_council_position = councillor_mage
	}
}

magic_mastery_value_good = {
	scope = character
	is_shown = {
		trigger_if = {
			limit = { has_variable = magic_mastery }
			var:magic_mastery = {
				compare_value >= 12
			}
		}
	}
}

magic_mastery_value = {
	scope = character
	is_shown = {
		trigger_if = {
			limit = { has_variable = magic_mastery }
			var:magic_mastery = {
				compare_value < 12
			}
		}
	}
}