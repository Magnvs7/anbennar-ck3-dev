﻿#Anbennar - magical affinity

#Anbennar
has_magical_affinity_trigger = {
	has_trait = magical_affinity
}

has_skaldic_tale_active = {
	OR = {
		has_character_modifier = skaldhyrric_dirge
		has_character_modifier = skaldhyrric_gjalund
		has_character_modifier = skaldhyrric_beralic
		has_character_modifier = skaldhyrric_voyage
		has_character_modifier = skaldhyrric_golden_forest
		has_character_modifier = skaldhyrric_master_boatbuilders
		has_character_modifier = skaldhyrric_dragon_and_skald
		has_character_modifier = skaldhyrric_old_winter_lullaby
	}
}