﻿#Events for blademarches succession
# trigger_event = X.1001

namespace = blademarches_succession

# 0001 - Assigns characters without a racial trait a trait according to their culture

# On gaining the title take the trial
blademarches_succession.0001 = {
	type = character_event
	title = blademarches_succession.0001.t
	desc = blademarches_succession.0001.desc
	theme = death
	left_portrait = root
	
	trigger = {
		NOT = {
			title:k_blademarches.holder = {
				has_character_modifier = wielded_calindal
				has_trait = bladeshunned
			}
		}
	}
	
	immediate = {
		# Clear all the bladestewards
		hidden_effect = {
			every_character = {
				limit = { has_character_modifier = bladesteward }
				remove_character_modifier = bladesteward
			}
		}
	}
	
	# Take the trial
	option = {
		name = blademarches_succession.0001.a
		trigger = {
			NOT = {
				# has_trait = blind
				has_character_modifier = blinded_by_calindal # Just in case there is a way to get un-blind
			}
			age > 14 # Must be at least 14 to take the trial
			# Must be able to access Calindal
			OR = {
				title:k_blademarches = {
					scope:owner = {
						any_character_artifact = {
							has_variable = is_calindal
						}
					}
				}
				any_character_artifact = {
					has_variable = is_calindal
				}
			}
		}
		trigger_event = blademarches_succession.0002
		show_as_tooltip = { blademarches_succession_determine_success = yes } # So players can see their chances of wiedling Calindal successfully
		stress_impact = {
			craven = massive_stress_gain
		}
		ai_chance = {
			base = 20
			ai_value_modifier = {
				ai_boldness = 1
			}
			ai_value_modifier = {
				ai_greed = 1
			}
			modifier = {
				factor = 0.25
				NOT = {
					AND = {
						root.martial = 5
						root.prowess = 5
					}
				}
			}
		}
	}
	
	# Decline to take the trial, if you gained the title you will keep it until someone else comes along to take it off you
	option = {
		name = blademarches_succession.0001.b
		trigger = {
			NOT = {
				# Can only refuse the trial if you don't already rule
				AND = {
					has_character_modifier = wielded_calindal
					has_title = title:k_blademarches
				}
			}
			# Must be able to access Calindal
			OR = {
				title:k_blademarches = {
					scope:owner = {
						any_character_artifact = {
							has_variable = is_calindal
						}
					}
				}
				any_character_artifact = {
					has_variable = is_calindal
				}
			}
		}
		blademarches_succession_reject_effect = yes
		stress_impact = {
			ambitious = massive_stress_gain
			brave = medium_stress_impact_gain
		}
		ai_chance = {
			base = 20
			# Take it if you're the heir
			modifier = {
				factor = 0
				has_title = title:k_blademarches
			}
			# Take it if you're very likely to succeed
			modifier = {
				factor = 0.5
				OR = {
					root.martial = 15
					root.prowess = 20
				}
				NOT = { has_trait = craven }
				NOT = { has_trait = content }
			}
			modifier = {
				factor = 0.25
				root.martial = 15
				root.prowess = 20
				NOT = { has_trait = craven }
				NOT = { has_trait = content }
			}
			# Less likely to not take if you're a vassal
			modifier = {
				factor = 0.75
				any_liege_or_above = {
					has_title = title:k_blademarches
				}
			}
			# Less likely if you've got a claim
			modifier = {
				factor = 0.75
				has_claim_on = title:k_blademarches
			}
			# Take it if you've wielded Calindal already
			modifier = {
				factor = 0
				has_character_modifier = wielded_calindal
			}
			# If you own it and have Bladestewards, have them wield it instead
			modifier = {
				factor = 0
				bladestewards_exist_trigger = yes
			}
		}
	}
	
	# Failure - Rule whilst blind
	option = {
		name = blademarches_succession.0001.c
		trigger = { 
			OR = {
				has_character_modifier = blinded_by_calindal
				has_trait = blind
			}
			has_title = title:k_blademarches
			# Must be able to access Calindal
			OR = {
				title:k_blademarches = {
					scope:owner = {
						any_character_artifact = {
							has_variable = is_calindal
						}
					}
				}
				any_character_artifact = {
					has_variable = is_calindal
				}
			}
		}
		add_trait_force_tooltip = bladeshunned
		add_prestige = major_prestige_loss
		add_tyranny = 20
		stress_impact = {
			just = medium_stress_impact_gain
			zealous = minor_stress_impact_gain
			arbitrary = minor_stress_impact_loss
		}
		
		ai_chance = {
			base = 10
			ai_value_modifier = {
				ai_boldness = 0.5
			}
			# If you own it and have Bladestewards, have them wield it instead
			modifier = {
				factor = 0
				bladestewards_exist_trigger = yes
			}
		}
	}
	
	# Have a Bladesteward wield Calindal
	option = {
		name = blademarches_succession.0001.d
		trigger = {
			has_title = title:k_blademarches
			NOT = { has_character_modifier = wielded_calindal }
			bladestewards_exist_trigger = yes
			# Must be able to access Calindal
			OR = {
				title:k_blademarches = {
					scope:owner = {
						any_character_artifact = {
							has_variable = is_calindal
						}
					}
				}
				any_character_artifact = {
					has_variable = is_calindal
				}
			}
		}
		
		add_prestige = medium_prestige_loss
		
		have_bladesteward_wield_calindal_effect = yes
		
		ai_chance = {
			base = 20
			# If you're not very brave
			ai_value_modifier = {
				ai_boldness = -1
			}
			# But you want the title
			ai_value_modifier = {
				ai_greed = 0.75
			}
		}
	}
	
	# Wait
	option = {
		name = blademarches_succession.0001.d
		trigger = {
			# Must be able to access Calindal
			NOR = {
				title:k_blademarches = {
					scope:owner = {
						any_character_artifact = {
							has_variable = is_calindal
						}
					}
				}
				any_character_artifact = {
					has_variable = is_calindal
				}
			}
		}
		
		if = {
			limit = {
				has_title = title:k_blademarches
			}
			add_character_modifier = untested_bladeking
		}
		
		ai_chance = {
			base = 1
		}
	}
}

# Determine success with the blade and decide what to do
blademarches_succession.0002 = {
	type = character_event
	title = blademarches_succession.0002.t
	desc = {
		# Attempt desc
		first_valid = {
			# Craven
			triggered_desc = {
				trigger = {
					has_trait = craven
				}
				desc = blademarches_succession.0002.attempt.craven
			}
			# Compassionate
			triggered_desc = {
				trigger = {
					has_trait = compassionate
				}
				desc = blademarches_succession.0002.attempt.compassionate
			}
			# Impatient
			triggered_desc = {
				trigger = {
					has_trait = impatient
				}
				desc = blademarches_succession.0002.attempt.impatient
			}
			# Humble AND Zealous but NOT cynical
			triggered_desc = {
				trigger = {
					OR = {
						has_trait = humble
						has_trait = zealous
					}
					NOT = { has_trait = cynical }
				}
				desc = blademarches_succession.0002.attempt.humble
			}
			# Brave OR Arrogant
			triggered_desc = {
				trigger = {
					OR = {
						has_trait = brave
						has_trait = arrogant
					}
				}
				desc = blademarches_succession.0002.attempt.brave
			}
			# Default
			desc = blademarches_succession.0002.attempt.default
		}
		# Success desc
		first_valid = {
			# Success
			triggered_desc = {
				trigger = {
					has_character_modifier = wielded_calindal
				}
				desc = {
					first_valid = {
						# Craven
						triggered_desc = {
							trigger = {
								has_trait = craven
							}
							desc = blademarches_succession.0002.success.craven
						}
						# Compassionate
						triggered_desc = {
							trigger = {
								has_trait = compassionate
							}
							desc = blademarches_succession.0002.success.compassionate
						}
						# Arrogant
						triggered_desc = {
							trigger = {
								has_trait = arrogant
							}
							desc = blademarches_succession.0002.success.arrogant
						}
						# Default
						desc = blademarches_succession.0002.success
					}
				}
			}
			# Failure
			triggered_desc = {
				trigger = {
					has_character_modifier = blinded_by_calindal
				}
				desc = {
					first_valid = {
						# Stubborn
						triggered_desc = {
							trigger = {
								OR = {
									has_trait = stubborn
									has_trait = arrogant
								}
							}
							desc = blademarches_succession.0002.failure.stubborn
						}
						# Greedy
						triggered_desc = {
							trigger = {
								has_trait = greedy
							}
							desc = blademarches_succession.0002.failure.greedy
						}
						# Craven
						triggered_desc = {
							trigger = {
								has_trait = craven
							}
							desc = blademarches_succession.0002.failure.craven
						}
						# Wrathful OR Vengeful
						triggered_desc = {
							trigger = {
								OR = {
									has_trait = vengeful
									has_trait = wrathful
								}
							}
							desc = blademarches_succession.0002.failure.wrathful
						}
						# Humble OR content
						triggered_desc = {
							trigger = {
								OR = {
									has_trait = humble
									has_trait = content
								}
							}
							desc = blademarches_succession.0002.failure.humble
						}
						# Default
						desc = blademarches_succession.0002.failure
					}
				}
			}
		}
	}
	theme = death
	left_portrait = {
		character = root
		# Just
		triggered_animation = {
			trigger = { 
				has_trait = just
				NOT = { has_character_modifier = wielded_calindal }
			}
			animation = shame
		}
		# Brave/Craven
		triggered_animation = {
			trigger = { 
				has_trait = brave
				has_character_modifier = wielded_calindal
			}
			animation = personality_bold
		}
		triggered_animation = {
			trigger = { 
				has_trait = craven
				has_character_modifier = wielded_calindal
			}
			animation = shock
		}
		# Greedy
		triggered_animation = {
			trigger = { 
				has_trait = greedy
				NOT = { has_character_modifier = wielded_calindal }
			}
			animation = grief
		}
		# Defaults
		triggered_animation = {
			trigger = {
				NOT = { has_character_modifier = wielded_calindal }
			}
			animation = pain
		}
		triggered_animation = {
			trigger = {
				has_character_modifier = wielded_calindal
			}
			animation = happiness
		}
	}
	
	immediate = {
		blademarches_succession_determine_success = yes
	}

	# Success
	option = {
		# Arrogant
		name = {
			trigger = { has_trait = arrogant }
			text = blademarches_succession.0002.a.arrogant
		}
		# Zealous
		name = {
			trigger = { has_trait = zealous }
			text = blademarches_succession.0002.a.zealous
		}
		# Ambitious
		name = {
			trigger = { has_trait = ambitious }
			text = blademarches_succession.0002.a.ambitious
		}
		# Default
		name = blademarches_succession.0002.a
		trigger = { has_character_modifier = wielded_calindal }
		if = {
			limit = {
				NOT = { has_title = title:k_blademarches }
			}
			add_prestige = medium_prestige_gain
			title:k_blademarches = {
				holder = {
					trigger_event = blademarches_succession.0003
				}
				create_title_and_vassal_change = {
					type = usurped
					save_scope_as = change
					add_claim_on_loss = no
				}
				change_title_holder_include_vassals = {
					holder = root
					change = scope:change
				}
				resolve_title_and_vassal_change = scope:change
			}
		}	
		else = {
			add_prestige = minor_prestige_gain
		}
	}
	
	# Failure - Abdicate
	option = {
		# Arrogant
		name = {
			trigger = { has_trait = arrogant }
			text = blademarches_succession.0002.b.arrogant
		}
		name = blademarches_succession.0002.b
		trigger = { 
			NOT = { has_character_modifier = wielded_calindal }
		}
		blademarches_succession_reject_effect = yes
		add_prestige = minor_prestige_loss
		stress_impact = {
			ambitious = medium_stress_impact_gain
			arrogant = medium_stress_impact_gain
			stubborn = minor_stress_impact_gain
			content = minor_stress_impact_loss
		}
		ai_chance = {
			base = 10
			ai_value_modifier = {
				ai_boldness = -0.5
			}
			ai_value_modifier = {
				ai_honor = 0.75
			}
			modifier = {
				factor = 0.1
				has_title = title:k_blademarches
				bladestewards_exist_trigger = yes
			}
		}
	}
	
	# Failure - Rule whilst blind
	option = {
		# Greedy
		name = {
			trigger = { has_trait = greedy }
			text = blademarches_succession.0002.c.greedy
		}
		# Compassionate
		name = {
			trigger = { has_trait = compassionate }
			text = blademarches_succession.0002.c.compassionate
		}
		name = blademarches_succession.0002.c
		trigger = { 
			NOT = { has_character_modifier = wielded_calindal } 
			has_title = title:k_blademarches 
		}
		add_trait_force_tooltip = bladeshunned
		add_prestige = major_prestige_loss
		add_tyranny = 20
		stress_impact = {
			just = medium_stress_impact_gain
			zealous = minor_stress_impact_gain
			arbitrary = minor_stress_impact_loss
		}
		
		ai_chance = {
			base = 10
			ai_value_modifier = {
				ai_boldness = 0.5
			}
			ai_value_modifier = {
				ai_greed = 0.5
			}
			modifier = {
				factor = 0
				bladestewards_exist_trigger = yes
			}
		}
	}
	
	# Failure - Have a Bladesteward wield
	option = {
		name = blademarches_succession.0002.d
		trigger = { 
			NOT = { has_character_modifier = wielded_calindal } 
			has_title = title:k_blademarches 
			bladestewards_exist_trigger = yes
		}
		
		have_bladesteward_wield_calindal_effect = yes
		
		ai_chance = {
			base = 50
		}
	}
}

# Lost Blademarches due to trial success
blademarches_succession.0003 = {
	type = character_event
	title = blademarches_succession.0003.t
	desc = blademarches_succession.0003.desc
	theme = death
	left_portrait = title:k_blademarches.holder
	
	# OK
	option = {
		name = blademarches_succession.0003.a
	}
}


# Inform ruler of Blademarches of the current wielder of Calindar
blademarches_succession.0004 = {
	type = character_event
	title = blademarches_succession.0004.t
	desc = blademarches_succession.0004.desc
	theme = death
	left_portrait = scope:calindal_wielder
	
	# OK
	option = {
		name = blademarches_succession.0004.a
	}
}

# Bladesteward has died
blademarches_succession.0005 = {
	type = character_event
	title = blademarches_succession.0005.t
	desc = blademarches_succession.0005.desc
	theme = death
	left_portrait = from
	
	# OK
	option = {
		name = blademarches_succession.0005.a
		trigger_event = blademarches_succession.0001
	}
}

# Try to wield Calindal without having the kingdom
blademarches_succession.0006 = {
	type = character_event
	title = blademarches_succession.0006.t
	desc = {
		# Attempt desc
		first_valid = {
			# Craven
			triggered_desc = {
				trigger = {
					has_trait = craven
				}
				desc = blademarches_succession.0006.attempt.craven
			}
			# Compassionate
			triggered_desc = {
				trigger = {
					has_trait = compassionate
				}
				desc = blademarches_succession.0006.attempt.compassionate
			}
			# Impatient
			triggered_desc = {
				trigger = {
					has_trait = impatient
				}
				desc = blademarches_succession.0006.attempt.impatient
			}
			# Humble AND Zealous but NOT cynical
			triggered_desc = {
				trigger = {
					OR = {
						has_trait = humble
						has_trait = zealous
					}
					NOT = { has_trait = cynical }
				}
				desc = blademarches_succession.0006.attempt.humble
			}
			# Brave OR Arrogant
			triggered_desc = {
				trigger = {
					OR = {
						has_trait = brave
						has_trait = arrogant
					}
				}
				desc = blademarches_succession.0006.attempt.brave
			}
			# Default
			desc = blademarches_succession.0006.attempt.default
		}
		# Success desc
		first_valid = {
			# Success
			triggered_desc = {
				trigger = {
					has_character_modifier = wielded_calindal
				}
				desc = {
					first_valid = {
						# Craven
						triggered_desc = {
							trigger = {
								has_trait = craven
							}
							desc = blademarches_succession.0006.success.craven
						}
						# Compassionate
						triggered_desc = {
							trigger = {
								has_trait = compassionate
							}
							desc = blademarches_succession.0006.success.compassionate
						}
						# Arrogant
						triggered_desc = {
							trigger = {
								has_trait = arrogant
							}
							desc = blademarches_succession.0006.success.arrogant
						}
						# Default
						desc = blademarches_succession.0006.success
					}
				}
			}
			# Failure
			triggered_desc = {
				trigger = {
					has_character_modifier = blinded_by_calindal
				}
				desc = {
					first_valid = {
						# Stubborn
						triggered_desc = {
							trigger = {
								OR = {
									has_trait = stubborn
									has_trait = arrogant
								}
							}
							desc = blademarches_succession.0006.failure.stubborn
						}
						# Greedy
						triggered_desc = {
							trigger = {
								has_trait = greedy
							}
							desc = blademarches_succession.0006.failure.greedy
						}
						# Craven
						triggered_desc = {
							trigger = {
								has_trait = craven
							}
							desc = blademarches_succession.0006.failure.craven
						}
						# Wrathful OR Vengeful
						triggered_desc = {
							trigger = {
								OR = {
									has_trait = vengeful
									has_trait = wrathful
								}
							}
							desc = blademarches_succession.0006.failure.wrathful
						}
						# Humble OR content
						triggered_desc = {
							trigger = {
								OR = {
									has_trait = humble
									has_trait = content
								}
							}
							desc = blademarches_succession.0006.failure.humble
						}
						# Default
						desc = blademarches_succession.0006.failure
					}
				}
			}
		}
	}
	theme = death
	left_portrait = {
		character = root
		# Just
		triggered_animation = {
			trigger = { 
				has_trait = just
				NOT = { has_character_modifier = wielded_calindal }
			}
			animation = shame
		}
		# Brave/Craven
		triggered_animation = {
			trigger = { 
				has_trait = brave
				has_character_modifier = wielded_calindal
			}
			animation = personality_bold
		}
		triggered_animation = {
			trigger = { 
				has_trait = craven
				has_character_modifier = wielded_calindal
			}
			animation = shock
		}
		# Greedy
		triggered_animation = {
			trigger = { 
				has_trait = greedy
				NOT = { has_character_modifier = wielded_calindal }
			}
			animation = grief
		}
		# Defaults
		triggered_animation = {
			trigger = {
				NOT = { has_character_modifier = wielded_calindal }
			}
			animation = pain
		}
		triggered_animation = {
			trigger = {
				has_character_modifier = wielded_calindal
			}
			animation = happiness
		}
	}
	
	immediate = {
		blademarches_succession_determine_success = yes
	}

	# Success
	option = {
		# Arrogant
		name = {
			trigger = { has_trait = arrogant }
			text = blademarches_succession.0006.a.arrogant
		}
		# Zealous
		name = {
			trigger = { has_trait = zealous }
			text = blademarches_succession.0006.a.zealous
		}
		# Ambitious
		name = {
			trigger = { has_trait = ambitious }
			text = blademarches_succession.0006.a.ambitious
		}
		# Default
		name = blademarches_succession.0006.a
		trigger = { has_character_modifier = wielded_calindal }
		if = {
			limit = {
				NOT = { has_title = title:k_blademarches }
			}
			add_prestige = medium_prestige_gain
			add_unpressed_claim = title:k_blademarches
		}	
		else = {
			add_prestige = minor_prestige_gain
		}
		ai_chance = {
			base = 1
		}
	}
	
	# Failure - Abdicate
	option = {
		# Arrogant
		name = {
			trigger = { has_trait = arrogant }
			text = blademarches_succession.0006.b.arrogant
		}
		name = blademarches_succession.0006.b
		trigger = { 
			NOT = { has_character_modifier = wielded_calindal }
		}
		add_prestige = minor_prestige_loss
		stress_impact = {
			ambitious = medium_stress_impact_gain
			arrogant = medium_stress_impact_gain
			stubborn = minor_stress_impact_gain
			content = minor_stress_impact_loss
		}
		ai_chance = {
			base = 1
		}
	}
}